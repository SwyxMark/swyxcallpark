# README #

A short demo video showing this feature in action can be found here https://youtu.be/XNSLIWk0zOM .

### What is this repository for? ###

This code is provided to act as a base for further development. It is not official code from Swyx and is not covered by Swyx Support.
The code has no special error handling and could be improved with additional functionality or made more efficient.
This script makes use of simple text files to stored call park information. This makes the installation and distribution of the code very simple and required very little configuration. For very large or busy systems, or systems requiring a larger number of park positions, it may be more advantageous to use a database rather than text files, but this will require further installation work and configuration tools to be developed. A project that could be taken on if the need arises. However form most companies this version is more than adequate for the role


### How do I get set up? ###
The callPark.rse file should be applied to call routing user as normal using the call routing manager.
The associated wav files should be placed in the users scope using the file manager in swyx admin (or the stand alone file manager tool)

The script will need editing
The source code can be found in the properties of the start block in the ecr
 

The first lines of code will be
' set this variable to a path on the server where the Swyx Service Account has read and write privlages
callParkPath = "d:\recordings\"

First time run / initialisation / reset
From any Swyx extension call the script adding 999 as postdial digits. 
For example if the script has been installed on routing user number 123 dial 123999 on Swyx It and press enter. Or dial 123999 on SwyxPhone and pick up the handset. 
DO NOT GO OFF HOOK THEN DIAL THE NUMBER
You will then be prompted for the reset password. 124816
The system will then initialise the park positions.
This can be confirmed by browsing the directory on the server where the park positions are stored and a number of text files will be found. 1 for each park position


